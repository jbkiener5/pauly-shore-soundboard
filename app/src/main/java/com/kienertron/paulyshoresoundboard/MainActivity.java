package com.kienertron.paulyshoresoundboard;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    MediaPlayer mediaPlayer1;
    MediaPlayer mediaPlayer2;
    MediaPlayer mediaPlayer3;
    MediaPlayer mediaPlayer4;
    MediaPlayer mediaPlayer5;
    MediaPlayer mediaPlayer6;
    MediaPlayer mediaPlayer7;
    MediaPlayer mediaPlayer8;
    MediaPlayer mediaPlayer9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button one = (Button) findViewById(R.id.button1);
            mediaPlayer1= MediaPlayer.create(this, R.raw.cheeza);
        Button two = (Button) findViewById(R.id.button2);
            mediaPlayer2= MediaPlayer.create(this, R.raw.viva_los_biodome);
        Button three = (Button) findViewById(R.id.button3);
            mediaPlayer3= MediaPlayer.create(this, R.raw.middle_america);
        Button four = (Button) findViewById(R.id.button4);
            mediaPlayer4= MediaPlayer.create(this, R.raw.im_a_weasel);
        Button five = (Button) findViewById(R.id.button5);
            mediaPlayer5= MediaPlayer.create(this, R.raw.hey_buddy);
        Button six = (Button) findViewById(R.id.button6);
            mediaPlayer6= MediaPlayer.create(this, R.raw.wheez_the_juice);
        Button seven = (Button) findViewById(R.id.button7);
            mediaPlayer7= MediaPlayer.create(this, R.raw.inbred);
        Button eight = (Button) findViewById(R.id.button8);
            mediaPlayer8= MediaPlayer.create(this, R.raw.grindage);
        Button nine = (Button) findViewById(R.id.button9);
            mediaPlayer9= MediaPlayer.create(this, R.raw.charisma);
    }
    public void cheeza(View one) {
        mediaPlayer1.start();
    }
    public void vivaLosBioDome(View two) {

        mediaPlayer2.start();
    }
    public void middleAmerica(View three) {

        mediaPlayer3.start();
    }
    public void imAWeasel(View four) {

        mediaPlayer4.start();
    }
    public void heyBuddy(View five) {

        mediaPlayer5.start();
    }
    public void wheezTheJuice(View six) {

        mediaPlayer6.start();
    }
    public void inbred(View seven) {

        mediaPlayer7.start();
    }
    public void grindage(View eight) {

        mediaPlayer8.start();
    }
    public void charisma(View nine) {

        mediaPlayer9.start();
    }
}

